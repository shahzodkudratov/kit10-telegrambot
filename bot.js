process.env.NTBA_FIX_319 = true;
const date = require('date-and-time');
const TelegramBot = require('node-telegram-bot-api');
const token = '576095096:AAFE6FRQKd2b3zWaVIuouFFWZnsCex2kw8E';
const bot = new TelegramBot(token, {polling: true});
const schedule = require('./resources/schedule');
const days = require('./resources/days');
const timetable = require('./resources/timetable');
const subjectno = require('./resources/subjectno');

bot.on('message', (msg) => {
  const chatId = msg.chat.id;
  const command = (/\/(\w*)(@kit10bot)?/.exec(msg.text))[1];
  const today = new Date();
  const yesterday = date.addDays(today, -1);
  const tomorrow = date.addDays(today, 1);

  /*=====================================
  COMMAND START
  =====================================*/

  if (command == "start") {

    response = `Привет\u{1F44B} Бот создан @shahzodkudratov для того, чтобы получать актуальное расписание уроков группы КИТ-10.

На текущий момент доступны 4 команды:
\u{1F449} /today - Показать расписание на сегодня
\u{1F449} /tomorrow - Показать расписание на завтра
\u{1F449} /week - Показать расписание на неделю
\u{1F449} /time - Показать время уроков`;

    //OUTPUT
    bot.sendMessage(chatId, response);
  } 

  /*=====================================
  COMMAND TODAY
  =====================================*/

  else if (command == "today") {
    getSchedule(today, (response) => {
      //OUTPUT
      bot.sendMessage(chatId, response);
    });
  }

  /*=====================================
  COMMAND TOMORROW
  =====================================*/

  else if (command == "tomorrow") {
    getSchedule(tomorrow, (response) => {
      //OUTPUT
      bot.sendMessage(chatId, response);
    });
  }

  /*=====================================
  COMMAND WEEK
  =====================================*/

  else if (command == "week") {
    response = "\n\n";
    for (i = 1; i < schedule.length; i++) {
      response += `\u{1F4DA} Расписание на ${days[i]}:\n`;

      for (j = 0; j < schedule[i].length; j++) {
        if(schedule[i][j].constructor === Array) {
          response += `${subjectno[j + 1].emoji} ${schedule[i][j][0].subject} или ${schedule[i][j][1].subject}\n`;
        } else {
          response += `${subjectno[j + 1].emoji} ${schedule[i][j].subject}\n`;
        }
      }

      response += "\n\n";
    }

    //OUTPUT
    bot.sendMessage(chatId, response);
  } 

  /*=====================================
  COMMAND TIME
  =====================================*/

  else if (command == "time") {
    response = "\n\n";
    for (i = 0; i < timetable.length; i++) {
      response += `${subjectno[i + 1].emoji} пара:\n`;
      response += `\u{1F55B} Начало: ${timetable[i].start}\n`;
      response += `\u{1F55E} Конец: ${timetable[i].end}\n`;
      response += "\n";
    }

    //OUTPUT
    bot.sendMessage(chatId, response);
  }

  /*=====================================
  LOG
  =====================================*/

  console.log(msg);
});

function getSchedule(date, callback){
  const id = date.getDay();
  let response;

  if(id == 0){
    response = `В ${days[id]} нет пар`;
  } else {
    response = `В ${days[id]} ${subjectno[schedule[id].length].full}:\n\n`;
    for (i = 0; i < schedule[id].length; i++) {
      if(schedule[id][i].constructor === Array) {
        response += `${subjectno[i + 1].emoji} пара:\n` +
                    `\u{1F4D3} ${schedule[id][i][0].full}\n` + 
                    `\u{1F55B} ${timetable[schedule[id][i][0].time - 1].start} - ${timetable[schedule[id][i][0].time - 1].end}\n` +
                    `\u{1F468}\u{200D}\u{1F3EB} ${schedule[id][i][0].teacher}\n\n` +
                    `или\n\n` + 
                    `\u{1F4D3} ${schedule[id][i][0].full}\n` + 
                    `\u{1F55B} ${timetable[schedule[id][i][0].time - 1].start} - ${timetable[schedule[id][i][0].time - 1].end}\n` +
                    `\u{1F468}\u{200D}\u{1F3EB} ${schedule[id][i][0].teacher}\n\n`;
      } else {
        response += `${subjectno[i + 1].emoji} пара:\n` +
                    `\u{1F4D3} ${schedule[id][i].full}\n` + 
                    `\u{1F55B} ${timetable[schedule[id][i].time - 1].start} - ${timetable[schedule[id][i].time - 1].end}\n` +
                    `\u{1F468}\u{200D}\u{1F3EB} ${schedule[id][i].teacher}\n\n`;
      }
    }
  }

  callback(response);
    
}